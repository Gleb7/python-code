from posts.models import Post

def my_processor(request):
	last_news = Post.objects.order_by('-created')[:5]
	context = {"last_news":last_news}
	return context