from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myBlog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^categories/', include('categories.urls', namespace="categories")),
    url(r'^polls/', include('polls.urls', namespace="polls")),
    url(r'^posts/', include('posts.urls', namespace="posts")),

)
