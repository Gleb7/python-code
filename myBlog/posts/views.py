from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response
from posts.models import Post, Comment
from django.views import generic
from forms import PostForm, CommentForm
from django.http import HttpResponseRedirect
from django.contrib.auth.models import AnonymousUser
import datetime
# Create your views here.

class IndexView(generic.ListView):
	template_name = 'posts/index.html'
	context_object_name = 'latest_posts_list'

	def get_queryset(self):
		return Post.objects.order_by('-created')[:5]

class DetailView(generic.DetailView, generic.edit.FormMixin):
    model = Post
    form_class = CommentForm
    template_name = 'posts/detail.html'

    def get_success_url(self):
        return reverse('posts:detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        form_class = self.get_form_class()
        comments = Comment.objects.filter(post=self.object.pk).order_by('-created')
        if self.request.user.is_authenticated():
           context['form'] = self.get_form(form_class)
        context['post_comments'] = comments
        return context
 
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
 
    def form_valid(self, form):
        form.cleaned_data['author'] = self.request.user
        form.cleaned_data['post'] = self.object
        form.cleaned_data['created'] = datetime.datetime.now()
        message = Comment(**form.cleaned_data)
        message.save()
        #message.process_files(self.request)
        return super(DetailView, self).form_valid(form)

def create(request):
    if request.POST:
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/posts/')
    else:
        form = PostForm()
        args = {}
        args.update(csrf(request))
        args['form'] = form
        return render_to_response('posts/create_posts.html', args)