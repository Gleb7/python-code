from django.conf import settings
from django.db import models
from categories.models import Category
# Create your models here.
class Post(models.Model):
     title = models.CharField(max_length = 200)
     text = models.TextField()
     created = models.DateTimeField('published date')
     categories = models.ManyToManyField(Category)

     def __unicode__(self):
     	return self.title

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    post = models.ForeignKey(Post)
    created = models.DateTimeField('commented at')
    comment = models.TextField()

    def __unicode__(self):
    	return self.comment