from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response
from posts.models import Post, Comment
from django.views import generic
from forms import PostForm, CommentForm
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
import datetime
# Create your views here.

class IndexView(generic.ListView):
	template_name = 'posts/index.html'
	context_object_name = 'latest_posts_list'

	def get_queryset(self):
		return Post.objects.order_by('-created')[:5]

'''def DetailView(request, post_id):
    post_comments = Comment.objects.filter(post=post_id).order_by('-created')
    post = Post.objects.get(pk=post_id)
    template_name = 'posts/detail.html'

    if request.POST:
        c = Comment(post=post, author=request.user, created=datetime.datetime.now())
        form = CommentForm(request.POST, instance = c)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/posts/'+post_id)
    else:
        form = CommentForm()
        args = {}
        args.update(csrf(request))
        args['form'] = form

    content = {'post_comments':post_comments, 'post':post, 'form': form, 'args': args}

    return render(request, template_name, content)'''
class DetailView(generic.DetailView, generic.edit.FormMixin):
    model = Post
    form_class = CommentForm
    template_name = 'posts/detail.html'

    def get_success_url(self):
        return reverse('posts:detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        form_class = self.get_form_class()
        comments = Comment.objects.filter(post=self.object.pk).order_by('-created')
        context['form'] = self.get_form(form_class)
        context['post_comments'] = comments
        return context
 
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
 
    def form_valid(self, form):
        form.cleaned_data['author'] = self.request.user
        form.cleaned_data['post'] = self.object
        form.cleaned_data['created'] = datetime.datetime.now()
        message = Comment(**form.cleaned_data)
        message.save()
        #message.process_files(self.request)
        return super(DetailView, self).form_valid(form)

def create(request):
    if request.POST:
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/posts/')
    else:
        form = PostForm()
        args = {}
        args.update(csrf(request))
        args['form'] = form
        return render_to_response('posts/create_posts.html', args)