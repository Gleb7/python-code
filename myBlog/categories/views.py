from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from categories.models import Category
from posts.models import Post
from django.views import generic
# Create your views here.

class IndexView(generic.ListView):
	template_name = 'categories/index.html'
	context_object_name = 'categories_list'

	def get_queryset(self):
		return Category.objects.order_by('-category')[:5]

def detail(request, category_id): # set to listView
	 category_posts = Post.objects.filter(categories=category_id)
	 category_name = Category.objects.get(pk=category_id)
	 content = {'category_posts':category_posts, 'category_name':category_name}
	 template_name = 'categories/detail.html'
	 return render(request, template_name, content)